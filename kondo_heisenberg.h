#ifndef __ITENSOR_KONDO_HEISENBERG_H
#define __ITENSOR_KONDO_HEISENBERG_H
#include "itensor/mps/siteset.h"

namespace itensor {

    class KondoHeisenbergSite;
    using KondoHeisenberg = BasicSiteSet<KondoHeisenbergSite>;

    class KondoHeisenbergSite
    {
    private:
        IQIndex s;

    public:
        KondoHeisenbergSite() { }
        KondoHeisenbergSite(IQIndex I) : s(I) { }

        KondoHeisenbergSite(int n, Args const& args = Args::global())
        {
            s = IQIndex{nameint("site=",n),
                  Index(nameint("0_U",n),1,Site),  QN("Sz=", 0,"Nf=",1),
                  Index(nameint("0_D",n),1,Site),  QN("Sz=", 0,"Nf=",1),
                  Index(nameint("U_U",n),1,Site),  QN("Sz=", 0,"Nf=",2),
                  Index(nameint("U_D",n),1,Site),  QN("Sz=", 0,"Nf=",2),
                  Index(nameint("D_U",n),1,Site),  QN("Sz=", 0,"Nf=",2),
                  Index(nameint("D_D",n),1,Site),  QN("Sz=", 0,"Nf=",2),
                  Index(nameint("UD_U",n),1,Site), QN("Sz=", 0,"Nf=",3),
                  Index(nameint("UD_D",n),1,Site), QN("Sz=", 0,"Nf=",3)};
        }

        IQIndex index() const
        {
            return s;
        }

        IQIndexVal state(std::string const& state)
        {
            if(state == "0" || state == "0_U"){ return s(1); }
            else if(state == "1" || state == "0_D"){ return s(2); }
            else if(state == "2" || state == "U_U"){ return s(3); }
            else if(state == "3" || state == "U_D"){ return s(4); }
            else if(state == "4" || state == "D_U"){ return s(5); }
            else if(state == "5" || state == "D_D"){ return s(6); }
            else if(state == "6" || state == "UD_U"){ return s(7); }
            else if(state == "7" || state == "UD_D"){ return s(8); }
            else {
                Error("State " + state + " not recognized");
            }

            return IQIndexVal{};
        }


        IQTensor op(std::string const& opname, Args const& args) const
        {
            auto sP = prime(s);

            IQTensor Op(dag(s),sP);

            if(opname == "c_0,u"){
                Op.set(s(1),sP(3),1);
                Op.set(s(2),sP(4),1);
                Op.set(s(5),sP(7),1);
                Op.set(s(6),sP(8),1);
            } else if(opname == "cT_0,u"){
                Op.set(s(3),sP(1),1);
                Op.set(s(4),sP(2),1);
                Op.set(s(7),sP(5),1);
                Op.set(s(8),sP(6),1);
            } else if(opname == "c_0,d"){
                Op.set(s(1),sP(5),1);
                Op.set(s(2),sP(6),1);
                Op.set(s(3),sP(7),1);
                Op.set(s(4),sP(8),1);
            } else if(opname == "cT_0,d"){
                Op.set(s(5),sP(1),1);
                Op.set(s(6),sP(2),1);
                Op.set(s(7),sP(3),1);
                Op.set(s(8),sP(4),1);

            } else if(opname == "n_0,ud"){
                Op.set(s(7),sP(7),1);
                Op.set(s(8),sP(8),1);

            } else if(opname == "s+_1"){
                Op.set(s(1),sP(5),1);
                Op.set(s(2),sP(6),1);
                Op.set(s(3),sP(7),1);
                Op.set(s(4),sP(8),1);
            } else if(opname == "s-_1"){
                Op.set(s(5),sP(1),1);
                Op.set(s(6),sP(2),1);
                Op.set(s(7),sP(3),1);
                Op.set(s(8),sP(4),1);
            } else if(opname == "sz_1"){
                Op.set(s(1),sP(1),1);
                Op.set(s(2),sP(2),1);
                Op.set(s(3),sP(3),1);
                Op.set(s(4),sP(4),1);
                Op.set(s(5),sP(5),-1);
                Op.set(s(6),sP(6),-1);
                Op.set(s(7),sP(7),-1);
                Op.set(s(8),sP(8),-1);

            } else if(opname == "s_01"){
                Op.set(s(3),sP(3),1);
                Op.set(s(6),sP(6),1);
                Op.set(s(4),sP(5),1);
                Op.set(s(5),sP(4),1);
                Op.set(s(4),sP(4),-1);
                Op.set(s(5),sP(5),-1);

            } else {
                Error("Operator \"" + opname + "\" name not recognized");
            }

            return Op;
        }
    };


} //namespace itensor

#endif
