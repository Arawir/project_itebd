#include <iostream>
#include <iomanip>
#include <fstream>
#include "itensor/all.h"
#include "itebd.h"
#include "kondo_heisenberg.h"

using namespace itensor;

#define out std::cout << std::fixed << std::setprecision(7)

int main ()
{
    double thres=1E-8,M=300, dt=0.002, T=1.0;
    double t00 = 3.0;
    double U = 1.0;
    double K = 1.0;
    double JH= 2.0;


    KondoHeisenberg sites(2);
    auto s1=sites(1), s2=sites(2);
    AutoMPO ampo(sites);

    ampo += t00,"cT_0,u",1,"c_0,u",2;
    ampo += t00,"c_0,u",1,"cT_0,u",2;
    ampo += t00,"cT_0,d",1,"c_0,d",2;
    ampo += t00,"c_0,d",1,"cT_0,d",2;

    ampo += U,"n_0,ud",1;
    ampo += U,"n_0,ud",2;

    ampo += K,"s+_1",1,"s-_1",2;
    ampo += K,"s-_1",1,"s+_1",2;
    ampo += K,"sz_1",1,"sz_1",2;

    ampo += JH,"s_01",1;
    ampo += JH,"s_01",2;


    itebd<IQIndex> Sys(ampo,2,3);


    out << "T :  E" << std::endl;
    while (Sys.tv.back()<T)  {
        std::cout << Sys.tv.back() << " "
                  << Sys.step_imag(dt,5,thres,M) << std::endl;
    }

    return 0;
}
